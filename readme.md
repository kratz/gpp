# Introduction

trying out generalized pair plots [1]. most examples use the diamond, petals or restaurant tips data sets. these are nice but how would a gpp look like on a big, real world dataset from genomics? the aim is to visualize the big table from kratz 2014. 

the resulting figure defies everything how figures "should" be, it is totally overloaded. yet I find it meaningful and... good. `

* why so much RNA in the ribosomal RNA group?

## what I did
Get the spreadsheet directly from Genome Research, and convert it into a csv file:
```bash
wget http://genome.cshlp.org/content/suppl/2014/06/05/gr.164095.113.DC1/Supplemental_Table.S3.xlsx
ssconvert Supplemental_Table.S3.xlsx Supplemental_Table.S3.csv
```

ssconvert is from gnumeric.

The R script reads in the csv file. Some columns contain only TRUE or FALSE and are parsed as logical. But in this context they need to be categorical So I use dplyr to convert them to factor.

scalelog2 is a helper function which I found on stackexchange. The problem is that ggpairs does not seem to be able to work with log scales. So this function steps through the matrix, retrieves the sub-plots and add log scales.

I then generate the ggpairs object, apply the scalelog2 function on the scatterplots and render this to PDF.

I invoke the R script with

```bash
Rscript ak.gpp.r
```

This PDF takes very long to render, so I convert it to a PNG in very high resolution:

```bash
convert -density 200 out.pdf out.png
```

convert is from ImageMagick.


# Interpretation 

Shown are two plots, one in clack and white and one where I did a further partition of the data according to the boolean tome variable.

![](composite.png)

# References

[1] Emerson, J. W. et al. The Generalized Pairs Plot. Journal of Computational and Graphical Statistics 22, 79–91 (2013).

