library(ggplot2)
library(GGally)
library(dplyr)

gr <- read.table("Supplemental_Table.S3.csv", header=TRUE,sep=",")

gr <- gr %>% mutate_if(is.logical,as.factor)

scalelog2 <- function(x=2,g) {  # for below diagonal
	for (i in 2:x){ 
		for (j in 1:(i-1)) { 
			g[i,(j)]<-g[i,(j)] + scale_x_continuous(trans='log2') + scale_y_continuous(trans='log2')
		}
	} 
	for (i in 1:x){  # for the bottom row 
		g[(x+1),i]<-g[(x+1),i] + scale_y_continuous(trans='log2') 
	} 
	for (i in 1:x){ # for the diagonal
		g[i,i]<-g[i,i]+ scale_x_continuous(trans='log2')
	} 
	return(g)
}


pm <- ggpairs(
	gr,
	mapping = aes(color = tome),
	columns = c("norm.41_Ctrl_60_MB_02", "norm.43_Ctrl_60_MUB_02", "norm.45_Ctrl_60_cyt_02",
	            "norm.47_Ctrl_60_cytUB_02", "biotype", "Doyle", "cajigas"),
	columnLabels = c("MB", "MUB", "cyt", "cytUB", "biotype", "Doyle", "Cajigas")
)

pm <- scalelog2(4, pm)

pdf(file="out.pdf", width=14, height=14)
pm + theme(axis.text.x = element_text(angle = 90, vjust = 1, color = "black"))
dev.off()
